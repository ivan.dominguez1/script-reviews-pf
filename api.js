import axios from 'axios'

const config = {
    headers: {
        Accept: "application/json",
        'Content-Type': "application/json",
        "X-VTEX-API-AppKey": "vtexappkey-pedidosfarma-LXOPYE",
        "X-VTEX-API-AppToken": "LBOYSAMABWCRMXKODYEJVTRESMOGMCZAWLNPCWJBATEITHPFOKOMDSUVDZCMYGDOBWFJDJZOVOBIYXBQTFYSAEEICSWHGHNEWMMWGBTRZDHQPXTFSPKMHJBZMRXRPFAT"
    }
}

function getProduct(productId) {
    const url = `https://pedidosfarma.vtexcommercestable.com.br/api/catalog/pvt/product/${productId}`
    return axios.get(url, config)
}

function getReviews({ Id, CategoryId, LinkId }) {
    const data = {
        "productId": Id,
        "categoryId": CategoryId,
        "productLinkId": LinkId,
        "qtdReviewsToBeShown": 500

    }

    return axios.post(`https://www.farmaplus.com.ar/userreview`, data, config)
}


function postReview(data) {
    const url = 'https://pedidosfarma.myvtex.com/reviews-and-ratings/api/reviews'
    return axios.post(url, data, config)
}


async function getListReviewsSaved(id) {

    try {
        const from = 0
        const to = 500
        const url = `https://pedidosfarma.myvtex.com/reviews-and-ratings/api/reviews?from=${from}&to=${to}&status=true&product_id=${id}`
        const { data: { data: list } } = await axios.get(url, config)

        if (list.length)
            return list.map(review => review.id)
        else
            return []
    } catch (e) {
        console.log('error', e)
        return []
    }

}


function deleteMasiveReview(reviewIds) {
    const url = `http://pedidosfarma.myvtex.com/reviews-and-ratings/api/reviews/`
    return axios.delete(url, {
        data: reviewIds,
        headers: config.headers
    })
}


export {
    getProduct, getReviews, postReview, getListReviewsSaved, deleteMasiveReview
}