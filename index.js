import { getProduct, getReviews, postReview } from './api.js'
import pkg from 'html2json'
const { html2json } = pkg;


const defaultTitles = ['Malo', 'Regular', 'Está bien', 'Bueno', 'Muy bueno']

const getTitle = (text, rating) => {
    if (text.length <= 40) return defaultTitles[rating - 1]
    else {
        let title = text.split(/[,.]/)[0]
        
        if(title.length > 70) return defaultTitles[rating - 1]
        else return title
    }
}

function getReviewsData(productId, reviews) {
    const jsonData = html2json(reviews)
    const list = jsonData.child[0].child[3].child[11].child[1].child

    const listReviews = list.filter(review => review.tag === 'li')
    const data = []
    for (let item of listReviews) {
        let user = 'anonimo'
        try {
            user = item.child[1].child[1].child[1].child[0].text.slice(15).split(' ')[0] || 'anonimo'
        } catch (e) {
            console.log('user dont exist')
        }
        let text = item.child[3].child[1].child[0].text
        let rating = parseInt(item.child[1].child[3].child[3].attr.class[1][1])
        let title = getTitle(text, rating) || "Sin titulo"

        data.push({
            "productId": productId,
            "rating": rating,
            "title": title,
            "text": text,
            "reviewerName": user,
            "verifiedPurchaser": false,
            "approved": true
        })
    }
    return data
}

for (let i = 1; i < 15602; i++) {
    try {
        console.log(`PRODUCT: ${i}`)
        const { data: product } = await getProduct(i)
        const { data: review } = await getReviews(product)
        const reviewsData = getReviewsData(i, review)
        await postReview(reviewsData)

    } catch (err) {
        console.log(err)
    }

}


process.exit()
