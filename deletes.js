import { getListReviewsSaved, deleteMasiveReview } from './api.js'


export default async () => {

    for (let i = 1; i < 15602; i++) {
        try {

            i = 1646
            console.log(`eliminando reviews de producto ${i}...`)
            const idsReviews = await getListReviewsSaved(i)

            let arrayToReviews = splitToChunks(idsReviews)
            for(let reviews of arrayToReviews) {
                const { data } = await deleteMasiveReview(reviews)
                await sleep(100)
                if (!data)
                throw new Error('error')

            }

            

            console.log(`reviews de producto ${i} eliminadas`)
        } catch (e) {
            i = -1
            console.log(`Error al eliminar reviews de producto ${i}, reintentando `)
        }
    }

}



function splitToChunks(array, parts) {
    let result = [];
    for (let i = parts; i > 0; i--) {
        result.push(array.splice(0, Math.ceil(array.length / i)));
    }
    return result;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}